# PetLove
Aplicativo baseado na API do ViaCEP

Caso queira ve-lo em produção, [veja uma demo ](http://branndonarruda.bitbucket.io/petlove/) aqui.
### Pre requisitos
- Node >= 11.14.0
- Angular 6.1.10
- Angular CLI: 6.1.5

## API em Node.js
Exemplo de requisição de endereço pela API criada em Node.
```cmd
https://petlove-app.herokuapp.com/?cep=01310100
```

## 1 - Baixe o Projeto
Faça o Download do Projeto ou clone ele
```cmd
git clone https://branndonarruda@bitbucket.org/branndonarruda/petlove.git
```

## 2 - Instale as Dependências
depois de baixar/clonar, entre dentro da pasta do projeto, e instale as dependências com o seguinte comando:
```cmd
npm install
```
## 3 - Rode o Projeto
Para dar build no projeto e visualizar a webapp em seu browser, rode o seguinte comando:
```cmd
ng serve
```
A webapp irá executar na porta 4200 do seu navegador.

## 4 - Finalizado
Para acessar a aplicação, acesse o endereço abaixo no seu navegador:
[http://localhost:4200](http://localhost:4200)

## 5 - Gerando o DIST para produção
Para gerar o DIST minificado para produção, em seu terminal rode o seguinte comando:
```cmd
ng build --prod --aot
```