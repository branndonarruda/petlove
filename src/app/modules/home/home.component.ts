import { Component, OnInit } from '@angular/core';
import { CepService } from '../../shared/services/cep.service';
import { CepInterface } from '../../shared/interface/cep.interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  addressUser: CepInterface[] = [];
  invalidCep = false;
  tempCep: String;

  constructor( private cepService: CepService ) { }

  ngOnInit() {}

  /**
   * funtion to return user address from CEP
   * @param e {event}
   * @param cep {string}
   */
  getAddress(e: Event, cep: String) {
    e.preventDefault();

    // if CEP equals before
    if ( cep === this.tempCep ) {
      return;
    }

    this.addressUser = [];

    if ( cep.length < 8 ) {
      this.invalidCep = true;
      return;
    }

    this.invalidCep = false;

    // request API
    this.cepService.getAddress(cep).subscribe(
      data => {
        this.tempCep = data.cep;
        data.cep ? this.addressUser.push(data) : this.invalidCep = true;
        console.log(this.addressUser);
      },
      err => {
        console.log('error API');
      }
    );
  }

}
