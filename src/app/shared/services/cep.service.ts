import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

import { CepInterface } from '../../shared/interface/cep.interface';
import { Observable } from '../../../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class CepService {

    constructor( private http: Http ) {}

    // get user address
    getAddress(cep: String): Observable<CepInterface> {
        return this.http.get('https://petlove-app.herokuapp.com/?cep=' + cep).pipe(
            map(response => response.json())
        );
    }
}
