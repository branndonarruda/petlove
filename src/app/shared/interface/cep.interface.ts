export interface CepInterface {
    bairro: String;
    cep: String;
    cidade: String;
    estado: String;
    rua: String;
}
