const express = require('express')
const app = express()
const port = process.env.PORT || 5000
const bodyParser = require('body-parser')
const cors = require('cors')
const request = require('request');

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors())

/**
 * API to return Address from CEP number
 */
app.get('/', function (req, res) {

  request('https://viacep.com.br/ws/' + req.query.cep + '/json/', function (error, response, body) {
    result = JSON.parse(body);

      if ( result.erro ) {
        return res.json({
          response: 400
        });
      }

      // Get Address fields
      res.json({
        cep: result.cep,
        estado: result.uf,
        cidade: result.localidade,
        bairro: result.bairro,
        logradouro: result.logradouro,
        response: response.statusCode
      });
  });
})

/**
 * Listen port develop only
 */
app.listen(port, function () {
  console.log(`Listening on http://localhost:${port}`)
})
